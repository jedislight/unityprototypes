using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomSpawner : MonoBehaviour
{

    public ScriptableObject currentGoal;
    public PlayerGameScript playerGameScript;
    public int roomCount;
    public Vector3 playerPos;
    public int tileGenerationMaxRange = 5;
    public int spawnPassesPerFrame = 5;
    public int openTransitions = 0;
    [HideInInspector]
    public List<GameObject> roomPrefabs = new List<GameObject>();
    [HideInInspector]
    public List<GameObject> spawnedRooms = new List<GameObject>();

    public List<Goal> goals = new List<Goal>();

    private Dictionary<string, RoomTileComponet> roomsByPosition = new System.Collections.Generic.Dictionary<string, RoomTileComponet>();

    public static string positionToKey(Vector3 position)
    {
        return string.Format("{0},{1},{2}", (int)position.x, (int)position.y, (int)position.z);
    }

    private GameObject addRoomAtPosition(RoomTileComponet neighbor, Vector3 position, GameObject prefab, int rotation)
    {
        GameObject room = Instantiate(prefab, position, Quaternion.Euler(0, rotation, 0));
        spawnedRooms.Add(room);
        room.GetComponent<RoomTileComponet>().rotate((int)rotation);
        roomsByPosition.Add(positionToKey(position), room.GetComponent<RoomTileComponet>());

        if (goals.Count > 0)
        {
            goals[0].onSpawned(this, position, room.GetComponent<RoomTileComponet>());
        }

        return room;
    }


    private void expandMapFrom(RoomTileComponet neighbor)
    {
        Vector3 position = neighbor.gameObject.transform.position;
        HashSet<int> openDirections = new HashSet<int>(){
            (int)TransitionsDirection.XN,
            (int)TransitionsDirection.XP,
            (int)TransitionsDirection.ZP,
            (int)TransitionsDirection.ZN,
            (int)TransitionsDirection.UP,
            (int)TransitionsDirection.DOWN
        };

        foreach (int direction in openDirections)
        {
            Vector3 spawnPosistion = nudgePosistionInDirection(direction, new Vector3(position.x, position.y, position.z));
            if (roomsByPosition.ContainsKey(positionToKey(spawnPosistion)))
            {
                continue;
            }

            List<SpawnCandidate> candidates = new List<SpawnCandidate>();
            foreach (GameObject roomPrefab in roomPrefabs)
            {
                for (int dm = 0; dm < 4; ++dm)
                {
                    SpawnCandidate candidate = new SpawnCandidate();
                    candidate.rotation = 90 * dm;
                    candidate.prefab = roomPrefab;
                    candidate.pos = spawnPosistion;
                    candidate.neighbor = neighbor;

                    candidates.Add(candidate);
                }
            }

            candidates = candidates.FindAll((c => isValidSpawn(c, (TransitionsDirection)direction)));

            if (candidates.Count > 0)
            {
                SpawnCandidate selected = candidates[Random.Range(0, candidates.Count)];
                addRoomAtPosition(selected.neighbor, selected.pos, selected.prefab, selected.rotation);
            }
        }
    }

    private bool isValidSpawn(SpawnCandidate c, TransitionsDirection exitDirection)
    {
        if (roomsByPosition.ContainsKey(c.posKey))
        {
            return false;
        }

        TransitionsDirection entranceDirection = RoomTileComponet.flip(exitDirection);
        if (!c.canEnterFrom(entranceDirection))
        {
            return false;
        }

        if (!c.canExitTo(exitDirection))
        {
            return false;
        }

        if (!checkGoalFilter(c))
        {
            return false;
        }

        return true;
    }

    private bool checkGoalFilter(SpawnCandidate c)
    {
        if (goals.Count > 0)
        {
            return goals[0].preSpawnFilter(this, c);
        }

        return true;
    }

    private static Vector3 nudgePosistionInDirection(int direction, Vector3 spawnPosistion)
    {
        switch (direction)
        {
            case (int)TransitionsDirection.XN: spawnPosistion.x -= 10; break;
            case (int)TransitionsDirection.XP: spawnPosistion.x += 10; break;

            case (int)TransitionsDirection.ZN: spawnPosistion.z -= 10; break;
            case (int)TransitionsDirection.ZP: spawnPosistion.z += 10; break;

            case (int)TransitionsDirection.UP: spawnPosistion.y += 10; break;
            case (int)TransitionsDirection.DOWN: spawnPosistion.y -= 10; break;
        }

        return spawnPosistion;
    }

    void expandMapByVisibilityFrom(RoomTileComponet neighbor, Vector3 cameraPos)
    {
        Vector3 position = neighbor.gameObject.transform.position;
        Vector3 TO_CENTER = new Vector3(0, 5, 0);
        Vector3 center = position + TO_CENTER;
        Vector3 targetCenter = center + new Vector3(
            10 * Random.Range(-tileGenerationMaxRange, tileGenerationMaxRange + 1),
            10 * Random.Range(-tileGenerationMaxRange, tileGenerationMaxRange + 1),
            10 * Random.Range(-tileGenerationMaxRange, tileGenerationMaxRange + 1));

        Vector3 targetPosition = targetCenter - TO_CENTER;
        string targetKey = positionToKey(targetPosition);
        if (!roomsByPosition.ContainsKey(targetKey))
        {
            return;
        }

        float visibilityOffset = Random.Range(0.0f, 4.99f);

        if (canSeeFrom(cameraPos, targetCenter)
        || canSeeFrom(cameraPos, targetCenter + new Vector3(-visibilityOffset, 0, 0))
        || canSeeFrom(cameraPos, targetCenter + new Vector3(visibilityOffset, 0, 0))
        || canSeeFrom(cameraPos, targetCenter + new Vector3(0, 0, -visibilityOffset))
        || canSeeFrom(cameraPos, targetCenter + new Vector3(0, 0, visibilityOffset))
        )
        {
            expandMapFrom(roomsByPosition[targetKey]);
        }
    }

    private bool canSeeFrom(Vector3 from, Vector3 to)
    {
        Vector3 rayDirection = to - from;
        float rayMax = rayDirection.magnitude;
        rayDirection = rayDirection.normalized;
        RaycastHit[] raycastHits = Physics.RaycastAll(from, rayDirection, rayMax);
        foreach (RaycastHit hit in raycastHits)
        {
            //Debug.Log("Discarding view from " + from + " to " + to + " hit object " + hit.collider.gameObject.name);
            if (GetComponent<LineRenderer>() != null)
            {
                GetComponent<LineRenderer>().SetPosition(0, from);
                GetComponent<LineRenderer>().SetPosition(1, to);
            }
            return false;
        }

        return true;
    }

    void Start()
    {
        List<Goal> workingGoals = new List<Goal>();
        foreach (Goal goal in goals)
        {
            workingGoals.Add(Instantiate(goal));
        }
        goals = workingGoals;

        startNextGoal();
    }

    void Update()
    {
        openTransitions = 0;

        if (isCurrentGoalFinished())
        {
            finishCurrentGoal();
            startNextGoal();
        }

        if (goals.Count > 0)
        {
            currentGoal = goals[0];
        }
        else
        {
            currentGoal = null;
        }

        // Update the room count text with the current number of spawned rooms
        roomCount = roomsByPosition.Count;

        // Get the player position
        playerPos = new Vector3(Mathf.Round(transform.position.x / 10.0f) * 10.0f, 0, Mathf.Round(transform.position.z / 10.0f) * 10.0f);

        string posKey = positionToKey(playerPos);
        RoomTileComponet parent = roomsByPosition.GetValueOrDefault(posKey, null);
        for (int i = 0; i < spawnPassesPerFrame; i++)
        {
            if (parent != null)
            {
                expandMapByVisibilityFrom(parent, transform.position);
            }
            else
            {
                Debug.Log("Placing root tile");
                addRoomAtPosition(null, playerPos, roomPrefabs[0], 0);
                break;
            }
        }
    }

    private bool isCurrentGoalFinished()
    {
        return goals.Count > 0 && goals[0].isComplete(this);
    }

    private void startNextGoal()
    {
        if (goals.Count > 0)
        {
            Debug.Log("Starting Goal " + goals[0].name);
            goals[0].onStart(this);
        }
    }

    private void finishCurrentGoal()
    {
        Debug.Log("Completed Goal " + goals[0].name);
        goals[0].onComplete(this);
        goals.RemoveAt(0);
    }
}
