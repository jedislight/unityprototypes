using System.Collections.Generic;

[System.Serializable]
    public class Transition {
        public TransitionsDirection from;
        public TransitionsDirection to;

        public List<string> constraints;
    }