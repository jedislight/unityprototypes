using UnityEngine;
public struct SpawnCandidate
{
    public GameObject prefab;
    public int rotation;
    public Vector3 pos;
    internal RoomTileComponet neighbor;

    public string posKey
    {
        get
        {
            return RoomSpawner.positionToKey(pos);
        }
    }

    public bool canEnterFrom(TransitionsDirection td)
    {
        int r = rotation;
        return prefab.GetComponent<RoomTileComponet>().transitions.Find(t => RoomTileComponet.rotateCW(t.from, r) == td) != null;
    }

    internal bool canExitTo(TransitionsDirection td)
    {
        return neighbor.GetComponent<RoomTileComponet>().transitions.Find(t => t.to == td) != null;
    }
}