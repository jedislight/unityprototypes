
using System.Collections.Generic;
using UnityEngine;

public abstract class Goal : ScriptableObject {
    public List<GameObject> tilePrefabs = new List<GameObject>();

    public virtual void onStart(RoomSpawner spawner){
        spawner.roomPrefabs.Clear();
        spawner.roomPrefabs.AddRange(tilePrefabs);
    }
    public virtual void onComplete(RoomSpawner spawner){}
    public abstract bool isComplete(RoomSpawner spawner);

    public virtual bool preSpawnFilter(RoomSpawner spawner, SpawnCandidate candidate) {
        return true;
    }

    public virtual void onSpawned(RoomSpawner spawner, Vector3 posistion, RoomTileComponet room) {}
}