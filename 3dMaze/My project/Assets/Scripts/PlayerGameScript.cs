using System;
using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerGameScript : MonoBehaviour
{
    public List<string> capabilities = new List<string>();
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other) {
        if(other.gameObject.name == "LevelEndTrigger") {
            SceneManager.LoadScene("Playground");
        }

        if(other.gameObject.name == "HiJumpGetTrigger") {
            FirstPersonController fpc = GetComponent<FirstPersonController>();
            fpc.JumpHeight = 12;
            GameObject.Destroy(other.gameObject);
            capabilities.Add("HiJump");
        }
    }

    internal bool hasCapabilities(List<string> constraints)
    {
        if(constraints.Count == 0) {
            return true;
        }

        foreach(string rawCon in constraints) {
            string[] cons = rawCon.Split(',');
            bool allMet = true;
            foreach(string con in cons) {
                if(!capabilities.Contains(con)) {
                    allMet = false;
                    break;
                }
            }

            if(allMet) {
                return true;
            }
        }

        return false;
    }
}
