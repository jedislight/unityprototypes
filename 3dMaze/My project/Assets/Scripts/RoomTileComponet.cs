using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomTileComponet : MonoBehaviour
{

    public List<Transition> transitions;

    public HashSet<int> getAllTransitions()
    {
        HashSet<int> h = new HashSet<int>();

        foreach (Transition t in transitions)
        {
            h.Add((int)t.to);
            h.Add((int)t.from);
        }
        return h;
    }

    public void rotate(int rotation)
    {
        for (int i = 0; i < rotation / 90; ++i)
        {
            rotateCW();
        }
    }

    private void rotateCW()
    {
        for (int i = 0; i < transitions.Count; ++i)
        {
            Transition transition = transitions[i];
            RoomTileComponet.rotateCW(transition);
        }
    }

    private static void rotateCW(Transition t)
    {
        t.to = rotateCW(t.to);
        t.from = rotateCW(t.from);
    }

    private static TransitionsDirection rotateCW(TransitionsDirection direction)
    {
        switch (direction)
        {
            case TransitionsDirection.ZP: return TransitionsDirection.XP;
            case TransitionsDirection.XN: return TransitionsDirection.ZP;
            case TransitionsDirection.ZN: return TransitionsDirection.XN;
            case TransitionsDirection.XP: return TransitionsDirection.ZN;
            default: return direction;
        }
    }

    public static TransitionsDirection flip(TransitionsDirection direction)
    {
        switch (direction)
        {
            case TransitionsDirection.ZP: return TransitionsDirection.ZN;
            case TransitionsDirection.XN: return TransitionsDirection.XP;
            case TransitionsDirection.ZN: return TransitionsDirection.ZP;
            case TransitionsDirection.XP: return TransitionsDirection.XN;
            case TransitionsDirection.UP: return TransitionsDirection.DOWN;
            case TransitionsDirection.DOWN: return TransitionsDirection.UP;
            default: return direction;
        }
    }

    public static TransitionsDirection rotateCW(TransitionsDirection direction, int rotation)
    {
        TransitionsDirection td = direction;
        for (int i = 0; i < rotation / 90; ++i)
        {
            td = rotateCW(td);
        }
        return td;
    }
}