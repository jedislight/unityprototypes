using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GoalRoomCount", order = 1)]

public class GoalRoomCount : Goal{

    public int target = 0;

    public override void onStart(RoomSpawner spawner)
    {
        base.onStart(spawner);
        target += spawner.openTransitions + spawner.spawnedRooms.Count;
    }
    public override bool isComplete(RoomSpawner spawner) {
        //Debug.Log(string.Format("GoalRoomCount.isComplete {0} >= {1}", spawner.openTransitions + spawner.spawnedRooms.Count, target));
        return spawner.openTransitions + spawner.spawnedRooms.Count >= target;
    }
}