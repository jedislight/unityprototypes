using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GoalCloseAndEnd", order = 1)]

public class GoalCloseAndEnd : Goal{
    public override bool isComplete(RoomSpawner spawner) {
        return false; // hold here forever , we are done
    }
}