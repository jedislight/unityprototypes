using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GoalSpawnExactlyOne", order = 1)]
public class GoalSpawnExactlyOne: Goal {
    private bool spawned;


    public override void onStart(RoomSpawner spawner){
        spawned = false;
        base.onStart(spawner);
    }

    public override bool preSpawnFilter(RoomSpawner spawner, SpawnCandidate candidate) {
        return !spawned;
    }

    public override void onSpawned(RoomSpawner spawner, Vector3 posistion, RoomTileComponet room) {
        Debug.Log("Goal Spawned at " + posistion + " in object " + room.gameObject.name);
        spawned = true;
    }

    public override bool isComplete(RoomSpawner spawner) {
        Debug.Log("Spawn and exit goal is complete: " + spawned);
        return spawned;
    }
}